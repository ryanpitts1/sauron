module CalendarHelper
  def calendar(date = Date.today, &block)
    Month.new(self, date, block).table
  end

  def week(date = Date.today, &block)
    Week.new(self, date, block).table
  end

  class Month < Struct.new(:view, :date, :callback)
    HEADER = %w[M T W TH F]
    START_DAY = :monday

    delegate :content_tag, to: :view

    def table
      content_tag :table, class: "calendar" do
        header + week_rows
      end
    end

    def header
      content_tag :tr do
        HEADER.map { |day| content_tag :th, day  }.join.html_safe
      end
    end

    def week_rows
      weeks.map do |week|
        content_tag :tr do
          week.map { |day| day_cell(day) unless day.strftime("%a") == "Sat" || day.strftime("%a") == "Sun" }.join.html_safe
        end
      end.join.html_safe
    end

    def day_cell(day)
      content_tag :td, view.capture(day, &callback), class: day_classes(day)
    end

    def day_classes(day)
      classes = []
      classes << "today" if day == Date.today
      classes << "notmonth" if day.month != date.month
      classes.empty? ? nil : classes.join(" ")
    end

    def weeks
      first = date.beginning_of_month.beginning_of_week(START_DAY)
      last = date.end_of_month.end_of_week(START_DAY)
      (first..last).to_a.in_groups_of(7)
    end
  end

  class Week < Struct.new(:view, :date, :callback)
    HEADER = %w[Mon Tue Wed Thu Fri]

    delegate :content_tag, to: :view

    def table
      content_tag :table, class: "calendar" do
        header + week_row
      end
    end

    def header
      content_tag :tr do
        HEADER.map { |day| content_tag :th, day }.join.html_safe
      end
    end

    def week_row
      content_tag :tr do
        week.map { |day| day_cell(day) unless day.strftime("%a") == "Sat" || day.strftime("%a") == "Sun" }.join.html_safe
      end
    end

    def day_cell(day)
      content_tag :td, view.capture(day, &callback), class: day_classes(day)
    end

    def day_classes(day)
      classes = []
      classes << "today" if day == Date.today
      classes << "notmonth" if day.month != date.month
      classes.empty? ? nil : classes.join(" ")
    end

    def week
      (date.at_beginning_of_week..date.at_end_of_week).to_a
    end
  end

  def group_events_by_availability(events, date)
    grouped_events = {}

    events.each do |event|
      if event.should_be_displayed(date)
        grouped_events[event.display_event_availability(date)] ||= []
        grouped_events[event.display_event_availability(date)] << event
      end
    end

    return grouped_events
  end

  def group_events_by_date(events)
    grouped_events = {}

    events.each do |event|
      grouped_events[event.start_date] ||= []
      grouped_events[event.start_date] << event
    end

    return grouped_events
  end

  def get_calendar_month_popover_data(events, date)
    date_events = []

    events.each do |event|
      if event.should_be_displayed(date)
        date_events << "<li class='event'><div class='inline-block availability #{event.display_event_availability(date)}'></div><div class='inline'>#{event.display_as_text(date, false)}</div></li>"
      end
    end

    popover_snippet = '<div class="popover events-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-events-heading">' + date.strftime("%^B") + ' ' + date.strftime("%d") + '</h3><div class="popover-content mbs mtn ptn"></div><div class="inline"><ul class="list-unstyled popover-events-list">' + date_events.join + '</ul></div></div>'
  end
end