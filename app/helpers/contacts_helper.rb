module ContactsHelper
  def current_availability(user)
    current_availability = ""
    date = Date.today
    events = Event.where("user_id = ?", user.id).collect
    events.each do |event|
      if event.should_be_displayed(date)
        case event.event_type
        when "In"
          return "yellow"
        when "Out"
          if (event.start_time != "12:00 am" || event.end_time != "12:00 am") && (event.start_date == date.strftime("%m/%d/%Y") || event.end_date == date.strftime("%m/%d/%Y"))
            return "yellow"
          else
            return "red"
          end
        when "Working from Home"
          return "green"
        when "Working from"
          return "green"
        when "on Vacation"
          if event.start_time != "12:00 am" || event.end_time != "12:00 am"
            return "yellow"
          else
            return "red"
          end
        end
      end
    end

    if current_availability != ""
      return current_availability
    else
      return "default"
    end
  end

  def upcoming_events_grouped_by_month(events)
    grouped_events = {}

    events.each do |event|
      date = Date.strptime(event.start_date, "%m/%d/%Y")
      month = date.strftime("%^b")

      if date >= Date.today
        grouped_events[month] ||= {}
        grouped_events[month][date] ||= []
        grouped_events[month][date] << event
      end

      if event.start_date != event.end_date
        grouped_events = get_multiple_dates(date, event, grouped_events)
      end
    end

    return grouped_events
  end

  def get_multiple_dates(date, event, grouped_events)
    date = date.tomorrow
    if date <= Date.strptime(event.end_date, "%m/%d/%Y")
      month = date.strftime("%^b")
      grouped_events[month] ||= {}
      grouped_events[month][date] ||= []
      grouped_events[month][date] << event
      grouped_events = get_multiple_dates(date, event, grouped_events)
    else
      return grouped_events
    end
  end
end