module ApplicationHelper
  def full_title(page_title)
      base_title = "Sauron"
      if page_title.empty?
          base_title
      else
          "#{page_title} | #{base_title}".html_safe
      end
  end

  def flash_class(level)
    case level
      when "notice" then "alert alert-info"
      when "success" then "alert alert-success"
      when "warning" then "alert alert-warning"
      when "error" then "alert alert-danger"
      when "alert" then "alert alert-danger"
    end
  end

  def nav_link(link_text, page)
    class_name = link_text == page ? "active" : ""

    content_tag(:li, :class => class_name) do
      link_to link_text, page
    end
  end

  def avatar_url(user)
    if user.profile_avatar.blank?
      "http://placehold.it/240x240/999999/333333&text=Avatar"
    else
      user.profile_avatar
    end
  end

  def current_weather
    barometer = Barometer.new("75069")
    weather = barometer.measure
    wind = weather.current.wind

    if wind.inspect.to_i > 20
      weather_icon = "windy"
    elsif ["Clear", "Cloudy", "MostlyCloudy", "MostlySunny", "PartlyCloudy", "PartlySunny", "Sunny"].include?(weather.current.condition) && ((Time.now > weather.current.sun.set) || (Time.now < weather.current.sun.rise))
      weather_icon = "moon"
    else
      weather_icon = weather.current.icon
    end

    @weather = {}
    @weather["icon"] = weather_icon
    @weather["condition"] = weather.current.condition
    @weather["temperature"] = weather.current.temperature.f
    @weather["full"] = weather
  end

  def weather_forecast
    barometer = Barometer.new("75013")
    weather = barometer.measure

    @forecast = weather.forecast
  end
end
