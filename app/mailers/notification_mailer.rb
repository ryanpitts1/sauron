class NotificationMailer < ActionMailer::Base
  include SendGrid
  sendgrid_category :use_subject_lines

  default from: "ryan@lifeblue.com"

  def event(event, event_text, user)
    sendgrid_category "Event Added"

    @event = event
    @event_text = event_text
    @user = user

    mail to: user.email, subject: "Sauron: #{event.user.display_name} has Added an Event to the Calendar"
  end

  def updated_event(event, event_text, user)
    sendgrid_category "Event Updated"

    @event = event
    @event_text = event_text
    @user = user

    mail to: user.email, subject: "Sauron: #{event.user.display_name} has Updated an Event on the Calendar"
  end
end
