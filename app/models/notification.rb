class Notification < ActiveRecord::Base
  belongs_to :user

  validates :notification_type, presence: true
  validates :event_type, presence: true
end