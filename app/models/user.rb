class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  mount_uploader :profile_banner, AssetUploader
  mount_uploader :profile_avatar, AssetUploader

  has_many :events, dependent: :destroy
  has_many :notifications, -> { order "id asc" }, dependent: :destroy

  accepts_nested_attributes_for :notifications, allow_destroy: true

  validates :display_name, presence: true
  validates :email, presence: true
end
