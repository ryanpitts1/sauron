require 'icalendar'

class Event < ActiveRecord::Base
  before_save :set_start_time_if_blank, :set_end_date_if_blank, :set_end_time_if_blank
  after_create :send_notifications
  after_update :send_update_notifications

  belongs_to :user

  validates :event_type, presence: true
  validates :start_date, presence: true

  def should_be_displayed(date)
    if Date.strptime(self.start_date, '%m/%d/%Y') == date || (Date.strptime(self.start_date, '%m/%d/%Y') < date && (Date.strptime(self.end_date, '%m/%d/%Y') == date || Date.strptime(self.end_date, '%m/%d/%Y') > date))
      return true
    else
      return false
    end
  end

  def display_event_availability(date)
    case self.event_type
    when "In"
      return "busy"
    when "Out"
      if (self.start_time != "12:00 am" || self.end_time != "12:00 am") && (Date.strptime(self.start_date, '%m/%d/%Y') == date || Date.strptime(self.end_date, '%m/%d/%Y') == date)
        return "busy"
      else
        return "unavailable"
      end
    when "Working from Home"
      return "available"
    when "Working from"
      return "available"
    when "on Vacation"
      if (self.start_time != "12:00 am" || self.end_time != "12:00 am") && (Date.strptime(self.start_date, '%m/%d/%Y') == date || Date.strptime(self.end_date, '%m/%d/%Y') == date)
        return "busy"
      else
        return "unavailable"
      end
    end
  end

  def display_as_text(date, snippet=true, plain_text=false)
    if snippet == true
      avatar = self.user.profile_avatar.blank? ? "http://placehold.it/240x240/999999/333333&text=Avatar" : self.user.profile_avatar
      popover_snippet = "tabindex=\'0\' role=\'button\' data-placement=\'top\' data-toggle=\'popover\' data-trigger=\'focus\' title=\'#{self.user.email}\' data-content=\'#{self.user.phone}\' data-template=\'<div class=\"popover profile-popover red\" role=\"tooltip\"><div class=\"arrow\"></div><a href=\"mailto:#{self.user.email}\" class=\"popover-title teal-link\"></a><div class=\"popover-content mbs mtn ptn\"></div><a href=\"/contacts/profile?u=#{self.user.id}\" class=\"button button-xs button-default\">View Full Profile</a><img class=\"profile-avatar\" src=\"#{avatar}\"></div>\'"
    else
      popover_snippet = false
    end

    if plain_text == true
      event_text = "#{self.user.display_name} is #{self.event_type.downcase}".html_safe
    else
      event_text = "<a href='#' #{popover_snippet}>#{self.user.display_name}</a> is #{self.event_type.downcase}".html_safe
    end

    case self.event_type
    when "In"
      if self.start_time != "12:00 am" && self.end_time == "12:00 am"
        event_text += " at #{self.start_time}".html_safe
      end
      if self.start_time == "12:00 am" && self.end_time != "12:00 am"
        event_text += " until #{self.start_time}".html_safe
      end
      return event_text
    when "Out"
      if !self.reason.blank?
        event_text += " #{self.reason}".html_safe
      end
      if self.start_time != "12:00 am" && Date.strptime(self.start_date, '%m/%d/%Y') == date
        event_text += " at #{self.start_time}".html_safe
      end
      if self.end_time != "12:00 am" && (Date.strptime(self.start_date, '%m/%d/%Y') == date || Date.strptime(self.start_date, '%m/%d/%Y') == Date.strptime(self.end_date, '%m/%d/%Y'))
        event_text += " until #{self.end_time}".html_safe
      end
      return event_text
    when "Working from Home"
      if self.start_time != "12:00 am" && Date.strptime(self.start_date, '%m/%d/%Y') == date
        event_text += " at #{self.start_time}".html_safe
      end
      if self.end_time != "12:00 am" && (Date.strptime(self.end_date, '%m/%d/%Y') == date || Date.strptime(self.start_date, '%m/%d/%Y') == Date.strptime(self.end_date, '%m/%d/%Y'))
        event_text += " until #{self.end_time}".html_safe
      end
      return event_text
    when "Working from"
      if !self.location.blank?
        event_text += " #{self.location}".html_safe
      end
      if self.start_time != "12:00 am" && Date.strptime(self.start_date, '%m/%d/%Y') == date
        event_text += " at #{self.start_time}".html_safe
      end
      if self.end_time != "12:00 am" && (Date.strptime(self.end_date, '%m/%d/%Y') == date || Date.strptime(self.start_date, '%m/%d/%Y') == Date.strptime(self.end_date, '%m/%d/%Y'))
        event_text += " until #{self.end_time}".html_safe
      end
      return event_text
    when "on Vacation"
      if self.start_time != "12:00 am" && Date.strptime(self.start_date, '%m/%d/%Y') == date
        event_text += " at #{self.start_time}".html_safe
      end
      if self.end_time != "12:00 am" && (Date.strptime(self.end_date, '%m/%d/%Y') == date || Date.strptime(self.start_date, '%m/%d/%Y') == Date.strptime(self.end_date, '%m/%d/%Y'))
        event_text += " until #{self.end_time}".html_safe
      end
      return event_text
    end
  end

  def notification_event_text
    case self.event_type
    when "In"
      event_text = "#{self.user.display_name} is #{self.event_type.downcase}".html_safe
      if self.start_time != "12:00 am" && self.end_time == "12:00 am"
        event_text += " at #{self.start_time}".html_safe
      end
      if self.start_time == "12:00 am" && self.end_time != "12:00 am"
        event_text += " until #{self.start_time}".html_safe
      end
      return event_text
    when "Out"
      event_text = "#{self.user.display_name} is #{self.event_type.downcase}".html_safe
      if !self.reason.blank?
        event_text += " #{self.reason}".html_safe
      end
      if self.start_date == self.end_date
        event_text += " on #{Date.strptime(self.start_date, "%m/%d/%Y").strftime("%B %e")}"
        if self.start_time != "12:00 am"
          event_text += " at #{self.start_time}".html_safe
        end
        if self.end_time != "12:00 am"
          event_text += " until #{self.end_time}".html_safe
        end
      else
        event_text += " from #{Date.strptime(self.start_date, "%m/%d/%Y").strftime("%B %e")}"
        if self.start_time != "12:00 am"
          event_text += " at #{self.start_time}".html_safe
        end
        event_text += " until #{Date.strptime(self.end_date, "%m/%d/%Y").strftime("%B %e")}"
        if self.end_time != "12:00 am"
          event_text += " at #{self.end_time}".html_safe
        end
      end
      return event_text
    when "Working from Home"
      event_text = "#{self.user.display_name} is #{self.event_type.downcase}".html_safe
      if self.start_date == self.end_date
        event_text += " on #{Date.strptime(self.start_date, "%m/%d/%Y").strftime("%B %e")}"
        if self.start_time != "12:00 am"
          event_text += " at #{self.start_time}".html_safe
        end
        if self.end_time != "12:00 am"
          event_text += " until #{self.end_time}".html_safe
        end
      else
        event_text += " from #{Date.strptime(self.start_date, "%m/%d/%Y").strftime("%B %e")}"
        if self.start_time != "12:00 am"
          event_text += " at #{self.start_time}".html_safe
        end
        event_text += " until #{Date.strptime(self.end_date, "%m/%d/%Y").strftime("%B %e")}"
        if self.end_time != "12:00 am"
          event_text += " at #{self.end_time}".html_safe
        end
      end
      return event_text
    when "Working from"
      event_text = "#{self.user.display_name} is #{self.event_type.downcase}".html_safe
      if !self.location.blank?
        event_text += " #{self.location}".html_safe
      end
      if self.start_date == self.end_date
        event_text += " on #{Date.strptime(self.start_date, "%m/%d/%Y").strftime("%B %e")}"
        if self.start_time != "12:00 am"
          event_text += " at #{self.start_time}".html_safe
        end
        if self.end_time != "12:00 am"
          event_text += " until #{self.end_time}".html_safe
        end
      else
        event_text += " from #{Date.strptime(self.start_date, "%m/%d/%Y").strftime("%B %e")}"
        if self.start_time != "12:00 am"
          event_text += " at #{self.start_time}".html_safe
        end
        event_text += " until #{Date.strptime(self.end_date, "%m/%d/%Y").strftime("%B %e")}"
        if self.end_time != "12:00 am"
          event_text += " at #{self.end_time}".html_safe
        end
      end
      return event_text
    when "on Vacation"
      event_text = "#{self.user.display_name} is #{self.event_type.downcase}".html_safe
      if !self.reason.blank?
        event_text += " #{self.reason}".html_safe
      end
      if self.start_date == self.end_date
        event_text += " on #{Date.strptime(self.start_date, "%m/%d/%Y").strftime("%B %e")}"
        if self.start_time != "12:00 am"
          event_text += " at #{self.start_time}".html_safe
        end
        if self.end_time != "12:00 am"
          event_text += " until #{self.end_time}".html_safe
        end
      else
        event_text += " from #{Date.strptime(self.start_date, "%m/%d/%Y").strftime("%B %e")}"
        if self.start_time != "12:00 am"
          event_text += " at #{self.start_time}".html_safe
        end
        event_text += " until #{Date.strptime(self.end_date, "%m/%d/%Y").strftime("%B %e")}"
        if self.end_time != "12:00 am"
          event_text += " at #{self.end_time}".html_safe
        end
      end
      return event_text
    end
  end

  def get_ics_summary
    return "#{self.user.display_name} is #{self.event_type.downcase} #{self.location} #{self.reason}".squish
  end

  def get_ics_start_date(tzid)
    if self.start_time == "12:00 am" && self.end_time != "12:00 am"
      self.start_time = "8:00 am"
    end

    if self.start_time == "12:00 am" && self.end_time == "12:00 am"
      event_start = Icalendar::Values::Date.new(DateTime.strptime("#{self.start_date}", "%m/%d/%Y").strftime("%Y%m%d"), 'tzid' => tzid)
    else
      event_start = Icalendar::Values::DateTime.new(DateTime.strptime("#{self.start_date} #{self.start_time}", "%m/%d/%Y %H:%M %p").strftime("%Y%m%dT%H%M%S"), 'tzid' => tzid)
    end

    return event_start
  end

  def get_ics_end_date(tzid)
    if self.end_time == "12:00 am" && self.start_time != "12:00 am"
      self.end_time = "5:00 pm"
    end

    if self.start_time == "12:00 am" && self.end_time == "12:00 am"
      if self.start_date < self.end_date
        end_date = DateTime.strptime("#{self.end_date}", "%m/%d/%Y") + 1.days
      else
        end_date = DateTime.strptime("#{self.end_date}", "%m/%d/%Y")
      end
      event_end = Icalendar::Values::Date.new(end_date.strftime("%Y%m%d"), 'tzid' => tzid)
    else
      event_end = Icalendar::Values::DateTime.new(DateTime.strptime("#{self.end_date} #{self.end_time}", "%m/%d/%Y %H:%M %p").strftime("%Y%m%dT%H%M%S"), 'tzid' => tzid)
    end

    return event_end
  end

  def send_notifications
    notifications = Notification.where("status = true").order("user_id ASC")
    notifications.each do |notification|
      if notification.user_id != self.user_id && notification.event_type == self.event_type && (notification.team == self.user.team || notification.users == self.user_id)
        NotificationMailer.event(self, self.notification_event_text, notification.user).deliver
      end
    end
  end

  def send_update_notifications
    notifications = Notification.where("status = true").order("user_id ASC")
    notifications.each do |notification|
      if notification.user_id != self.user_id && notification.event_type == self.event_type && (notification.team == self.user.team || notification.users == self.user_id)
        NotificationMailer.updated_event(self, self.notification_event_text, notification.user).deliver
      end
    end
  end

  private
    def set_start_time_if_blank
      if self.start_time.blank?
        self.start_time = "12:00 am"
      end
    end

    def set_end_date_if_blank
      if self.end_date.blank?
        self.end_date = self.start_date
      end
    end

    def set_end_time_if_blank
      if self.end_time.blank?
        self.end_time = "12:00 am"
      end
    end
end