class SlackController < ApplicationController

  def message
    greeting = [
      "Ok",
      "Sure thing",
      "You got it"
    ].sample

    if params[:text].include? " help"
      message = "These are the things I can do:" + "\n" + ">>>"
      message += "#cal or #cal today = I'll show you today's events" + "\n"
      message += "#cal tomorrow = I'll show you tomorrow's events" + "\n"
      message += "#cal this week = I'll show you this week's events" + "\n"
    elsif params[:text].include? " today"
      event_count = 0
      events = Event.all.order("start_time asc")
      message = "#{greeting}, #{params[:user_name]}. Here's the schedule for today:" + "\n"

      events.each do |event|
        date = Date.today

        if event.should_be_displayed(date)
          event_text = event.display_as_text(date, false, true)

          message += ">" + event_text + "\n"

          event_count += 1
        end
      end

      if event_count > 4
        message += [
          "You people need to get to work, slackers!",
          "Wow, does anything ever get done around there?",
          "Umm, where is everyone going?"
        ].sample
      elsif event_count == 0
        message = "Whoa. No one's out? Great work-life balance, y'all!"
      end
    elsif params[:text].include? " tomorrow"
      event_count = 0
      events = Event.all.order("start_time asc")
      message = "#{greeting}, #{params[:user_name]}. Here's what tomorrow looks like:" + "\n"

      events.each do |event|
        date = Date.tomorrow

        if event.should_be_displayed(date)
          event_text = event.display_as_text(date, false, true)

          message += ">" + event_text + "\n"

          event_count += 1
        end
      end

      if event_count > 4
        message += [
          "You people need to get to work, slackers!",
          "Wow, does anything ever get done around there?",
          "Umm, where is everyone going?"
        ].sample
      elsif event_count == 0
        message = "Whoa. No one's out? Great work-life balance, y'all!"
      end
    elsif params[:text].include? " this week"
      events = Event.all.order("start_time asc")
      dates = (Date.today..Date.today.at_end_of_week-2).to_a

      message = "#{greeting}, #{params[:user_name]}. Here is what's happening the rest of this week:" + "\n" + ">>>"

      dates.each do |date|
        message += "*" + date.strftime("%b %-d").upcase + "*" + "\n"

        events.each do |event|
          if event.should_be_displayed(date)
            event_text = event.display_as_text(date, false, true)

            message += event_text + "\n"
          end
        end

        message += "\n"
      end
    else
      message = "Uh, I have no idea what you just said. Try asking for help."
    end

    render json: { text: message }
  end
end