class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :new_event
  layout :layout_by_resource

  private
    def layout_by_resource
      if ((controller_name == "registrations" && (action_name == "new" || action_name == "create")) || controller_name == "sessions" || controller_name == "passwords" || controller_name == "confirmations" || controller_name == "unlocks")
        "authentication"
      else
        "application"
      end
    end

    def after_sign_in_path_for(resource)
      stored_location_for(resource) || week_calendar_path
    end

    def after_sign_out_path_for(resource_or_scope)
      sign_in_path
    end

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:id, :display_name, :phone, :profile_banner, :profile_avatar, :team, :quote, :is_admin, :email, :password, :password_confirmation, :current_password, notifications_attributes: [:id, :notification_type, :event_type, :team, :users, :status, :_destroy]) }
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:id, :display_name, :team, :is_admin, :email, :password, :password_confirmation, :current_password) }
    end

    def new_event
      @event = Event.new
      @times = ["", "12:00 am", "12:30 am", "1:00 am", "1:30 am", "2:00 am", "2:30 am", "3:00 am", "3:30 am", "4:00 am", "4:30 am", "5:00 am", "5:30 am", "6:00 am", "6:30 am", "7:00 am", "7:30 am", "8:00 am", "8:30 am", "9:00 am", "9:30 am", "10:00 am", "10:30 am", "11:00 am", "11:30 am", "12:00 pm", "12:30 pm", "1:00 pm", "1:30 pm", "2:00 pm", "2:30 pm", "3:00 pm", "3:30 pm", "4:00 pm", "4:30 pm", "5:00 pm", "5:30 pm", "6:00 pm", "6:30 pm", "7:00 pm", "7:30 pm", "8:00 pm", "8:30 pm", "9:00 pm", "9:30 pm", "10:00 pm", "10:30 pm", "11:00 pm", "11:30 pm"]
    end
end
