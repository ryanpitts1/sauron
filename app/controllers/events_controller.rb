class EventsController < ApplicationController
  before_action :authenticate_user!

  def create
    @event = Event.new(event_params)
    if @event.save
      flash[:success] = "Your event has been successfully created!"
      redirect_to :back
    else
      if @event.start_date.blank?
        flash[:error] = "Event start date is required. Try creating your event again."
      else
        flash[:error] = "Something went wrong! Please try again."
      end
      redirect_to :back
    end
  end

  def edit
    @event = Event.find(params[:id])

    render json: @event
  end

  def update
    @event = Event.find(params[:id])
    if @event.update(event_params)
      flash[:success] = "Your event has been successfully updated!"
      redirect_to :back
    else
      flash.now[:error] = "Something went wrong! Please try again."
      redirect_to :back
    end
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    flash[:warning] = "An event for #{@event.user.display_name} has been deleted."
    redirect_to :back
  end

  private
    def event_params
      params.require(:event).permit(:user_id, :event_type, :reason, :location, :start_date, :end_date, :start_time, :end_time, :notes, :status, :_destroy)
    end
end