require 'icalendar'
require 'icalendar/tzinfo'

class CalendarController < ApplicationController
  before_action :authenticate_user!, except: [:export]

  def index
    @date = params[:date] ? Date.parse(params[:date]) : Date.today
    @year = params[:date] ? Date.parse(params[:date]).year : Date.today.year
    @events = Event.all.order("start_time asc")
    @this_week_dates = (Date.today.at_beginning_of_week..Date.today.at_end_of_week-2).to_a
  end

  def week
    @date = params[:date] ? Date.parse(params[:date]) : Date.today
    @events = Event.all.order("start_time asc")
  end

  def export
    @events = Event.all
    @calendar = Icalendar::Calendar.new

    tzid = "America/Chicago"
    tz = TZInfo::Timezone.get tzid
    timezone = tz.ical_timezone DateTime.now
    @calendar.add_timezone timezone

    @events.each do |e|
      event = Icalendar::Event.new

      event.dtstart = e.get_ics_start_date(tzid)
      event.dtend = e.get_ics_end_date(tzid)
      event.summary = e.get_ics_summary
      event.description = e.notification_event_text
      event.location = e.location

      @calendar.add_event(event)
    end

    @calendar.publish

    respond_to do |format|
      format.ics do
        render :text => @calendar.to_ical
      end
    end
  end
end