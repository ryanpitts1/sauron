class ReportingController < ApplicationController
  before_action :authenticate_user!

  def index
    @events_wfh = Event.where(event_type: "Working from Home").group_by(&:user_id)
  end
end