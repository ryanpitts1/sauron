class WeatherController < ApplicationController

  def message
    weather = Wunderground.new("404e2b48ae674406")
    forecast = weather.forecast_for("TX", "McKinney")
    conditions = weather.conditions_for("75069")
    message = ""

    if params[:text].include? " help"
      message = "These are the things I can do:" + "\n" + ">>>"
      message += "#weather now = I'll show you the current weather" + "\n"
      message += "#weather today = I'll show you today's forecast" + "\n"
      message += "#weather tomorrow = I'll show you tomorrow's forecast"
    elsif params[:text].include? " now"
      message += forecast['forecast']['txt_forecast']['forecastday'][0]['fcttext'] + "\n" + ">>>"
      message += "*Condition:* #{conditions['current_observation']['weather']}" + "\n"
      message += "*Temp:* #{conditions['current_observation']['temp_f']} F" + "\n"
      message += "*Wind:* #{conditions['current_observation']['wind_string']}" + "\n"
      message += "*Humidity:* #{conditions['current_observation']['relative_humidity']}" + "\n"
      message += "*Precipitation Chances Today:* #{forecast['forecast']['txt_forecast']['forecastday'][0]['pop']}%"
    elsif params[:text].include? " today"
      message += "*Today:* " + forecast['forecast']['txt_forecast']['forecastday'][0]['fcttext'] + "\n" + ">>>"
      message += "*Condition:* #{forecast['forecast']['simpleforecast']['forecastday'][0]['conditions']}" + "\n"
      message += "*High:* #{forecast['forecast']['simpleforecast']['forecastday'][0]['high']['fahrenheit']} F" + "\n"
      message += "*Low:* #{forecast['forecast']['simpleforecast']['forecastday'][0]['low']['fahrenheit']} F" + "\n"
      message += "*Wind:* #{forecast['forecast']['simpleforecast']['forecastday'][0]['maxwind']['dir']} #{forecast['forecast']['simpleforecast']['forecastday'][0]['maxwind']['mph']}" + "\n"
      message += "*Humidity:* #{forecast['forecast']['simpleforecast']['forecastday'][0]['avehumidity']}%" + "\n"
      message += "*Precipitation Chances Today:* #{forecast['forecast']['simpleforecast']['forecastday'][0]['pop']}%"
    elsif params[:text].include? " tomorrow"
      message += "*Tomorrow:* " + forecast['forecast']['txt_forecast']['forecastday'][1]['fcttext'] + "\n" + ">>>"
      message += "*Condition:* #{forecast['forecast']['simpleforecast']['forecastday'][1]['conditions']}" + "\n"
      message += "*High:* #{forecast['forecast']['simpleforecast']['forecastday'][1]['high']['fahrenheit']} F" + "\n"
      message += "*Low:* #{forecast['forecast']['simpleforecast']['forecastday'][1]['low']['fahrenheit']} F" + "\n"
      message += "*Wind:* #{forecast['forecast']['simpleforecast']['forecastday'][1]['maxwind']['dir']} #{forecast['forecast']['simpleforecast']['forecastday'][1]['maxwind']['mph']}" + "\n"
      message += "*Humidity:* #{forecast['forecast']['simpleforecast']['forecastday'][1]['avehumidity']}%" + "\n"
      message += "*Precipitation Chances Today:* #{forecast['forecast']['simpleforecast']['forecastday'][1]['pop']}%"
    else
      message = "Uh, I have no idea what you just said. Try asking for help."
    end

    render json: { text: message }
  end
end