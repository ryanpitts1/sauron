class StaticPagesController < ApplicationController
  before_action :authenticate_user!, only: :dashboard

  def home
  end

  def dashboard
    @date = Date.today
    @events = Event.all
  end

  def contacts
    @users = User.all.order("display_name ASC")
  end

  def profile
    @user = User.find(params[:u])
    events = Event.where(user_id: params[:u]).order("start_date ASC")

    @events = []
    events.each do |event|
      if Date.strptime(event.start_date, "%m/%d/%Y") >= Date.today || (Date.strptime(event.start_date, "%m/%d/%Y") <= Date.today && Date.strptime(event.end_date, "%m/%d/%Y") >= Date.today)
        @events << event
      end
    end
  end
end