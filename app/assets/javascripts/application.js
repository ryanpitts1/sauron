//= require jquery
//= require bootstrap-sprockets
//= require jquery_nested_form
//= require jquery_ujs
//= require jquery-ui
//= require_tree .

$(function() {
    if ($("[data-toggle='popover']").length > 0) {
        $("[data-toggle='popover']").popover();
    }

    if ($("#expand-trigger").length > 0) {
        $("#expand-trigger").on("click", function(event) {
            $("#expand-target").toggleClass("expanded");
        });
    }

    if ($(".contacts-grid-view").length > 0) {
        $(".show-grid-view").on("click", function(event) {
            event.preventDefault();
            $(".contacts-list-view").addClass("hidden");
            $(".show-list-view").removeClass("teal-link");
            $(".contacts-grid-view").removeClass("hidden");
            $(".show-grid-view").addClass("teal-link");
        });
        $(".show-list-view").on("click", function(event) {
            event.preventDefault();
            $(".contacts-grid-view").addClass("hidden");
            $(".show-grid-view").removeClass("teal-link");
            $(".contacts-list-view").removeClass("hidden");
            $(".show-list-view").addClass("teal-link");
        });

        $(".show-lifeblue").on("click", function(event) {
            event.preventDefault();
            $(".team-filter a").removeClass("teal-link");
            $(".team-filter .show-lifeblue").addClass("teal-link");
            $("[data-user-team]").css("opacity","1");
        });
        $(".show-1400").on("click", function(event) {
            event.preventDefault();
            $(".team-filter a").removeClass("teal-link");
            $(".team-filter .show-1400").addClass("teal-link");
            $("[data-user-team]").css("opacity","1");
            $("[data-user-team][data-user-team!='1400']").css("opacity",".2");
        });
        $(".show-prism").on("click", function(event) {
            event.preventDefault();
            $(".team-filter a").removeClass("teal-link");
            $(".team-filter .show-prism").addClass("teal-link");
            $("[data-user-team]").css("opacity","1");
            $("[data-user-team][data-user-team!='Prism']").css("opacity",".2");
        });
        $(".show-flux").on("click", function(event) {
            event.preventDefault();
            $(".team-filter a").removeClass("teal-link");
            $(".team-filter .show-flux").addClass("teal-link");
            $("[data-user-team]").css("opacity","1");
            $("[data-user-team][data-user-team!='Flux']").css("opacity",".2");
        });
    }

    if ($(".edit-profile-banner").length > 0) {
        var $profileBanner = $(".edit-profile-banner"),
            $profileBannerField = $(".profile-banner-field"),
            $profileAvatar = $(".edit-profile-avatar"),
            $profileAvatarField = $(".profile-avatar-field");

        $profileBanner.on("click", function() {
            $profileBannerField.trigger("click");
        });
        $profileBannerField.change(function(event) {
            var input = $(event.currentTarget),
                file = input[0].files[0],
                reader = new FileReader();

            reader.onload = function(e){
                image_base64 = e.target.result;
                $profileBanner.css("background-image", "url(" + image_base64 + ")");
                $profileBanner.addClass("pending-changes");
                $profileBanner.find(".profile-banner-edit-overlay").css("top", "100%").css("transform", "translate(-50%)");
                $profileBanner.find(".profile-banner-pending-overlay").css("top", "50%").css("transform", "translate(-50%, -175%)");
            };

            reader.readAsDataURL(file);
        });

        $profileAvatar.on("click", function() {
            $profileAvatarField.trigger("click");
        });
        $profileAvatarField.change(function(event) {
            var input = $(event.currentTarget),
                file = input[0].files[0],
                reader = new FileReader();

            reader.onload = function(e){
                image_base64 = e.target.result;
                $profileAvatar.css("background-image", "url(" + image_base64 + ")");
                $profileAvatar.addClass("pending-changes");
                $profileAvatar.find(".profile-avatar-edit-overlay").css("top", "100%").css("transform", "translate(-50%)");
                $profileAvatar.find(".profile-avatar-pending-overlay").css("top", "50%").css("transform", "translate(-50%, -50%)");
            };

            reader.readAsDataURL(file);
        });
    }
});