require "rails_helper"

feature "User Sign Out" do
  let!(:user) { Fabricate(:user) }
 
  background(:each) do
    login_as(user, scope: :user)
  end

  scenario "displays the sign in page after signing out" do
    visit sign_out_path

    expect(page).to have_content("Sign In")
  end
end