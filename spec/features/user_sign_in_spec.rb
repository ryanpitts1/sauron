require "rails_helper"

feature "User Sign In" do
  let!(:user) { Fabricate(:user) }

  scenario "displays the calendar week view after signing in" do
    visit sign_in_path

    find("#user_email").set(user.email)
    find("#user_password").set(user.password)

    click_button "Sign In"

    expect(page).to have_content("Previous Week")
    expect(page).to have_content("Next Week")
  end
end