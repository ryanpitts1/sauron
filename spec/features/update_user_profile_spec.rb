require "rails_helper"

feature "User updates their account" do
  let!(:user) { Fabricate(:user) }

  background(:each) do
    login_as(user, scope: :user)
  end

  scenario "the user sees a success message" do
    visit edit_user_registration_path

    find("#user_display_name").set("Test User")
    find("#user_current_password").set(user.password)

    click_button "Update"

    user.reload

    expect(page).to have_content("Your account has been successfully updated!")
    expect(user.display_name).to eq("Test User")
  end

  scenario "the user receives an error if the account update was unsuccessful" do
    visit edit_user_registration_path

    find("#user_email").set("")
    find("#user_current_password").set(user.password)

    click_button "Update"

    expect(page).to have_content("prohibited this user from being saved")
    expect(page).to have_content("Email can't be blank")
  end
end