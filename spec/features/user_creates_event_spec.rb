require "rails_helper"

feature "User creates an event" do
  let!(:user) { Fabricate(:user) }

  background(:each) do
    login_as(user, scope: :user)
  end

  scenario "the user sees a success message" do
    visit calendar_path

    click_link "Add Event"

    select('Out', from: 'event_event_type')
    find("#event_start_date").set("09/15/2015")

    click_button "Create Event"

    expect(page).to have_content("Your event has been successfully created!")
  end

  # scenario "the user receives an error if the account update was unsuccessful" do
  #   visit edit_user_registration_path

  #   find("#user_email").set("")
  #   find("#user_current_password").set(user.password)

  #   click_button "Update"

  #   expect(page).to have_content("prohibited this user from being saved")
  #   expect(page).to have_content("Email can't be blank")
  # end
end