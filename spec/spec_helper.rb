# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration
require 'devise'

RSpec.configure do |config|
  config.include Devise::TestHelpers, :type => :controller
  config.include Warden::Test::Helpers
  Warden.test_mode!
end