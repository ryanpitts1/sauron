require "rails_helper"

RSpec.describe NotificationMailer, :type => :mailer do
  describe "created event" do
    let!(:event) { Fabricate(:event) }
    let(:mail) { NotificationMailer.event(event, event.notification_event_text, event.user) }

    it "renders the headers" do
      expect(mail.subject).to eq("Sauron: #{event.user.display_name} has Added an Event to the Calendar")
      expect(mail.to).to eq([event.user.email])
      expect(mail.from).to eq(["ryan@lifeblue.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to have_content("Just wanted to let you know")
    end
  end

  describe "updated event" do
    let!(:event) { Fabricate(:event) }
    let(:mail) { NotificationMailer.updated_event(event, event.notification_event_text, event.user) }

    it "renders the headers" do
      expect(mail.subject).to eq("Sauron: #{event.user.display_name} has Updated an Event on the Calendar")
      expect(mail.to).to eq([event.user.email])
      expect(mail.from).to eq(["ryan@lifeblue.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to have_content("Just wanted to let you know")
    end
  end

end
