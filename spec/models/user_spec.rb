require "rails_helper"

describe User do
  let!(:user) { Fabricate(:user) }

  it "has a valid factory" do
    expect(user).to be_valid
  end

  it "is invalid without a display name" do
    user.display_name = nil

    expect(user).to_not be_valid
    expect(user.errors[:display_name]).to include("can't be blank")
  end

  it "is invalid without an email" do
    user.email = nil

    expect(user).to_not be_valid
  end

  it "is invalid with an invalid email" do
    user.email = "test@example"

    expect(user).to_not be_valid
  end
end