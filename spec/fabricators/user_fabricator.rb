Fabricator(:user) do
  email { Faker::Internet.email }
  password { "password" }
  display_name { Faker::Name.name }
  is_admin { true }
end