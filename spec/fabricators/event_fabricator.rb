Fabricator(:event) do
  user { Fabricate(:user) }
  event_type { "In" }
  reason { "" }
  location { "" }
  start_date { "03/15/2015" }
  end_date { "" }
  start_time { "10:00 am" }
  end_time { "" }
  notes { "" }
  status { true }
end