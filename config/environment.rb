# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!

ActionMailer::Base.smtp_settings = {
  :address => "smtp.sendgrid.net",
  :port => 587,
  :authentication => :plain,
  :enable_starttls_auto => true,
  :domain => Figaro.env.sendgrid_domain,
  :user_name => Figaro.env.sendgrid_username,
  :password => Figaro.env.sendgrid_password
}

ActionMailer::Base.smtp_settings[:enable_starttls_auto] = false if Rails.env.production?