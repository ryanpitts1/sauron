Rails.application.routes.draw do
  devise_for :users, controllers: {registrations: 'registrations'}

  devise_scope :user do
    root to: "devise/sessions#new"
    get "sign-in", to: "devise/sessions#new"
    get "sign-out", to: "devise/sessions#destroy"
  end

  get "dashboard" => "static_pages#dashboard"
  get "contacts" => "static_pages#contacts"
  get "contacts/profile" => "static_pages#profile"

  resource "calendar" do
    get "/" => "calendar#index"
    get "week" => "calendar#week"
    get "selected-date" => "calendar#selected_date"
    get "feed" => "calendar#export"
  end

  resources :events

  resource "reporting" do
    get "/" => "reporting#index"
  end

  resource "slack" do
    post "message" => "slack#message"
  end

  resource "weather" do
    post "message" => "weather#message"
  end
end