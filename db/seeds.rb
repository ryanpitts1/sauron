require "faker"

if User.count == 0
  puts "Adding users to the Users table..."
  User.create!(email: "melany@lifeblue.com", password: "password", display_name: "Melany", team: "Lifeblue", is_admin: true)
  User.create!(email: "shyam@lifeblue.com", password: "password", display_name: "Shyam", team: "Lifeblue", is_admin: true)
  User.create!(email: "russel@lifeblue.com", password: "password", display_name: "Russel", team: "Lifeblue", is_admin: true)
  User.create!(email: "phillip@lifeblue.com", password: "password", display_name: "Phillip", team: "1400", is_admin: true)
  User.create!(email: "derek@lifeblue.com", password: "password", display_name: "Derek", team: "1400", is_admin: true)
  User.create!(email: "johnny@lifeblue.com", password: "password", display_name: "Johnny", team: "1400", is_admin: true)
  User.create!(email: "john@lifeblue.com", password: "password", display_name: "John", team: "1400", is_admin: true)
  User.create!(email: "brian@lifeblue.com", password: "password", display_name: "Brian", team: "1400", is_admin: true)
  User.create!(email: "ryan@lifeblue.com", password: "password", display_name: "Ryan Pitts", team: "Prism", is_admin: true)
  User.create!(email: "lauren@lifeblue.com", password: "password", display_name: "Lauren", team: "Prism", is_admin: true)
  User.create!(email: "daniel@lifeblue.com", password: "password", display_name: "Daniel", team: "Prism", is_admin: true)
  User.create!(email: "trevor@lifeblue.com", password: "password", display_name: "Trevor", team: "Prism", is_admin: true)
  User.create!(email: "jimmy@lifeblue.com", password: "password", display_name: "Jimmy", team: "Prism", is_admin: true)
  User.create!(email: "david@lifeblue.com", password: "password", display_name: "David", team: "Skyblue", is_admin: true)
  User.create!(email: "chris@lifeblue.com", password: "password", display_name: "Chris", team: "Skyblue", is_admin: true)
  User.create!(email: "mandy@lifeblue.com", password: "password", display_name: "Mandy", team: "Skyblue", is_admin: true)
  puts "...done!"
end

User.create!(email: "travis@lifeblue.com", password: "password", display_name: "Travis", team: "Skyblue", is_admin: true)