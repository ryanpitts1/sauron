class CreateNotifications < ActiveRecord::Migration
  def self.up
    create_table :notifications do |t|
      t.integer :user_id,             default: 0, null: false
      t.string :notification_type,    default: '', null: false, limit: 250
      t.string :event_type,           default: '', null: false, limit: 250
      t.string :team,                 default: '', null: false, limit: 250
      t.string :users,                default: '', limit: 1000
      t.boolean :status,              default: true, null: false

      t.timestamps
    end

    add_index :notifications, :user_id
  end

  def self.down
    remove_index :notifications, :user_id

    drop_table :notifications
  end
end
