class CreateEvents < ActiveRecord::Migration
  def self.up
    create_table :events do |t|
      t.integer :user_id,           default: 0, null: false
      t.string :event_type,         default: '', limit: 250
      t.string :reason,             default: '', limit: 250
      t.string :location,           default: '', limit: 250
      t.string :start_date,         default: '', limit: 250
      t.string :end_date,           default: '', limit: 250
      t.string :start_time,         default: '', limit: 250
      t.string :end_time,           default: '', limit: 250
      t.string :notes,              default: '', limit: 2000
      t.boolean :status,            default: true, null: false

      t.timestamps
    end

    add_index :events, :user_id
  end

  def self.down
    remove_index :events, :user_id

    drop_table :events
  end
end